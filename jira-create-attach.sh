#!/bin/bash

set -e

key=$(bash jira.sh ${1}| jq -r '.key')

echo ${key}

rand=$(openssl rand -hex 8)

split_prefix="split_${rand}"

split --verbose -b 1G "${1}" "${split_prefix}"

for f in ${split_prefix}*
do
	echo "Uploading ${f}..."
	curl -s --request POST \
	  --url "https://bitchass.atlassian.net/rest/api/3/issue/${key}/attachments" \
	  --user 'jkel3766@gmail.com:oapymUtsLGgv03GF2rF0A599' \
	  --header 'X-Atlassian-Token: no-check' \
	  -F "file=@$f" &
done

FAIL=0

for job in `jobs -p`
do
   wait $job || let "FAIL+=1"
done

echo $FAIL

if [ "$FAIL" == "0" ];
then
    echo "YAY!"
else
    echo "FAIL! ($FAIL)"
fi

rm -v ${split_prefix}*
