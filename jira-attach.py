import requests
from requests.auth import HTTPBasicAuth
import json
import sys
from multiprocessing import Process

FILE=sys.argv[1]

def create_issue():
    payload = json.dumps( {
      "fields": {
        "summary": f"{FILE}",

        "issuetype": {
          "name": "Task"
        },
        "project": {
          "key": "TEST"
        },
        "description": {
          "type": "doc",
          "version": 1,
          "content": [
            {
              "type": "paragraph",
              "content": [
                {
                  "text": "Order entry fails when selecting supplier.",
                  "type": "text"
                }
              ]
            }
          ]
        },
      }
    } )

    response = requests.request(
       "POST",
       "https://bitchass.atlassian.net/rest/api/3/issue",
       data=payload,
       headers={'Accept': 'application/json','Content-Type': 'application/json'},
       auth=HTTPBasicAuth("jkel3766@gmail.com", "oapymUtsLGgv03GF2rF0A599")
    )

    print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")))
    return json.loads(response.text)["key"]


def upload(data, filename, ISSUE_ID):
    response = requests.request(
            "POST",
            f"https://bitchass.atlassian.net/rest/api/3/issue/{ISSUE_ID}/attachments",
            headers={"Accept": "application/json", "X-Atlassian-Token": "no-check"},
            auth=HTTPBasicAuth("jkel3766@gmail.com", "oapymUtsLGgv03GF2rF0A599"),
            files={"file": (filename, data, "application-type")},
        )
    print(f"======================={filename}=====================")
    print(
            json.dumps(
                json.loads(response.text),
                sort_keys=True,
                indent=4,
                separators=(",", ": "),
            )
        )



chunk_size = 256*1024*1024  # 1 GB

if len(sys.argv) < 3:
    ISSUE_ID=create_issue()
    print(ISSUE_ID)

with open(FILE, "rb") as in_file:
    counter=0
    procs=[]
    while True:
        chunk = in_file.read(chunk_size)

        if chunk == b"":
            break  # end of file
        counter+=1

        proc = Process(target=upload, args=(chunk, f"{FILE}.{counter}", ISSUE_ID))  # instantiating without any argument
        procs.append(proc)
        proc.start()

    for p in procs:
        p.join()
